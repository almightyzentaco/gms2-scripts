///@desc Convert an index value into a x,y position and return as array
///@arg index
///@arg columns

var index = argument0;
var columns = argument1;

var gridarray = array_create(2,0)
gridarray[0] = index % columns;
gridarray[1] = floor(index/columns);

return gridarray;

