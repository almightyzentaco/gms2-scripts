///@desc find lowest gamepad
// """ Sometimes a gamepad will register itself with a slot higher than 0, even if it is the        """
// """ only gamepad plugged in. This script will find the lowest active gamepad slot and return it. """
//-------------------------------------------------------------------------------------------------------


//init vars
var gpid = noone;
var gp_num = gamepad_get_device_count();

//find lowest
for (var i = 0; i < gp_num; i++;){
	if gamepad_is_connected(i){
	   gpid = i;
	   break;
   }
}

return gpid;