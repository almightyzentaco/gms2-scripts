///@arg gamepad ID
gp_id = argument0;
//Keyboard
if (!gamepad_is_connected(gp_id)){
	key_right = keyboard_check(bnd_key_right);
	key_right_pressed = keyboard_check_pressed(bnd_key_right);
	key_left = keyboard_check(bnd_key_left);
	key_left_pressed = keyboard_check_pressed(bnd_key_left);
	key_up = keyboard_check(bnd_key_up);
	key_up_pressed = keyboard_check_pressed(bnd_key_up);
	key_down = keyboard_check(bnd_key_down);
	key_down_pressed = keyboard_check_pressed(bnd_key_down);
	key_action_1 = keyboard_check(bnd_key_action_1);
	key_action_1_pressed = keyboard_check_pressed(bnd_key_action_1);
	key_action_1_released = keyboard_check_released(bnd_key_action_1);
	key_action_2 = keyboard_check(bnd_key_action_2);
	key_action_2_pressed = keyboard_check_pressed(bnd_key_action_2);
	key_action_2_released = keyboard_check_released(bnd_key_action_2);
	key_action_3 = keyboard_check(bnd_key_action_3);
	key_action_3_pressed = keyboard_check_pressed(bnd_key_action_3);
	key_action_3_released = keyboard_check_released(bnd_key_action_3);
	key_action_4 = keyboard_check(bnd_key_action_4);
	key_action_4_pressed = keyboard_check_pressed(bnd_key_action_4);
	key_action_4_released = keyboard_check_released(bnd_key_action_4);
	key_action_5 = keyboard_check(bnd_key_action_5);
	key_action_5_pressed = keyboard_check_pressed(bnd_key_action_5);
	key_action_5_released = keyboard_check_released(bnd_key_action_5);
}
if (gamepad_is_connected(gp_id)){
	key_up = gamepad_axis_value(gp_id,gp_axislv)<-gp_dz;
	key_down = gamepad_axis_value(gp_id,gp_axislv)>gp_dz;
	key_right = gamepad_axis_value(gp_id,gp_axislh)>gp_dz;
	key_left = gamepad_axis_value(gp_id,gp_axislh)<-gp_dz;
	#region axis check pressed
	//-------------------------------------------------------------------
	//clear keycheck states
	key_up_pressed = 0;
	key_down_pressed = 0;
	key_right_pressed = 0;
	key_left_pressed = 0;
	//axis check pressed for
	if axischeck_up == 0{
		if (key_up != 0){
			key_up_pressed = 1;
			axischeck_up = 1;
		}
	}
	if axischeck_down == 0{
		if (key_down != 0){
			key_down_pressed = 1;
			axischeck_down = 1;
		}	
	}
	if axischeck_right == 0{
		if (key_right !=0) {
			key_right_pressed = 1;
			axischeck_right = 1;
		}
	}
	if axischeck_left == 0{
		if (key_left != 0) {
			key_left_pressed = 1;
			axischeck_left = 1;
		}	
	}
	//clear on release for more pushes
	if (key_up == 0) axischeck_up = 0;
	if (key_down == 0) axischeck_down = 0;
	if (key_right == 0) axischeck_right = 0;
	if (key_left == 0) axischeck_left = 0;
	//----------------------------------------------------------------------
	#endregion
	key_action_1 = gamepad_button_check(gp_id,bnd_pad_action_1);
	key_action_1_pressed = gamepad_button_check_pressed(gp_id,bnd_pad_action_1);
	key_action_1_released = gamepad_button_check_released(gp_id,bnd_pad_action_1);
	key_action_2 = gamepad_button_check(gp_id,bnd_pad_action_2);
	key_action_2_pressed = gamepad_button_check_pressed(gp_id,bnd_pad_action_2);
	key_action_2_released = gamepad_button_check_released(gp_id,bnd_pad_action_2);
	key_action_3 = gamepad_button_check(gp_id,bnd_pad_action_3);
	key_action_3_pressed = gamepad_button_check_pressed(gp_id,bnd_pad_action_3);
	key_action_3_released = gamepad_button_check_released(gp_id,bnd_pad_action_3);
	key_action_4 = gamepad_button_check(gp_id,bnd_pad_action_4);
	key_action_4_pressed = gamepad_button_check_pressed(gp_id,bnd_pad_action_4);
	key_action_4_released = gamepad_button_check_released(gp_id,bnd_pad_action_4);
	key_action_5 = gamepad_button_check(gp_id,bnd_pad_action_5);
	key_action_5_pressed = gamepad_button_check_pressed(gp_id,bnd_pad_action_5);
	key_action_5_released = gamepad_button_check_released(gp_id,bnd_pad_action_5);
	

}

//get directionals
xmove = key_right - key_left;
ymove = key_down - key_up;