///@desc Return true if mouse clicked on specific object
///@arg id
///@arg mb
//---------------------------------------------------------------

//initialize
var obj_id = argument0;
var mb = argument1;

//check for mouse click
if position_meeting(mouse_x, mouse_y, obj_id)
{
	if mouse_check_button_pressed(mb)
	{
		return true;	
	}
}
else
{
	return false;	
}