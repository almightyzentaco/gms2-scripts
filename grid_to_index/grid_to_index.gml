///@desc Convert a grid position into an index value 
///(eg: convert an inventory slot into an index of a single dimensional array)
///@arg x
///@arg y
///@arg columns

var gridx = argument0;
var gridy = argument1;
var columns = argument2;

var index = (gridy * columns) + gridx;
return index;