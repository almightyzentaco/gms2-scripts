///@desc Convert a gamepad buttonpress constant into a string
///@arg button

var button = argument0;

if (button == gp_face1) return "A";
if (button == gp_face2) return "B";
if (button == gp_face3) return "X";
if (button == gp_face4) return "Y";
if (button == gp_shoulderl) return "Left Bumper";
if (button == gp_shoulderlb) return "Left Trigger";
if (button == gp_shoulderr) return "Right Bumper";
if (button == gp_shoulderrb) return "Right Trigger";
if (button == gp_select) return "Select";
if (button == gp_start) return "Start";
if (button == gp_stickl) return "Lstick Press";
if (button == gp_stickr) return "Rstick Press";
if (button == gp_padu) return "Gpad Up";
if (button == gp_padd) return "Gpad Down";
if (button == gp_padl) return "Gpad Left";
if (button == gp_padr) return "Gpad Right";
if (button == gp_axislh) return "Left Stick Horiz";
if (button == gp_axislv) return "Left Stick Vert";
if (button == gp_axisrh) return "Right Stick Horiz";
if (button == gp_axisrv) return "Right Stick Vert";
