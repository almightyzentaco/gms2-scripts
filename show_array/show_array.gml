///@arg array index
///@arg xffset
///@arg yoffset
///@arg margin size

//set arguments
var array = argument0;
var xoffset = argument1;
var yoffset = argument2;
var border = argument3;

//show values
var height = array_height_2d(array);
var width = array_length_2d(array,0);
var i = 0; var ii = 0;
repeat(height){
	repeat(width){
	draw_text(border+(ii*xoffset),border+(i*yoffset),string(array[i,ii]));	
	ii++;
	}
	ii=0;
	i++;
}

