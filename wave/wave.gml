///desc do this thing
///@arg center
///@arg amp
///@arg spd

var center = argument0;
var amp = argument1;
var spd = argument2;
//initialize wavetime
if !variable_instance_exists(id,"wavetime") wavetime = 0;

wavetime++;
return(center+sin(wavetime*spd)*amp)


