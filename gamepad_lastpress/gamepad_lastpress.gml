///@desc return the last button pressed
///@arg gpad_id
///@arg true=keycode:false=NameAsString

var gpad_id = argument0;
var keycode = argument1;

#region buttons array
var button_list = array_create(16,0);
button_list[0] = gp_face1;
button_list[1] = gp_face2;
button_list[2] = gp_face3;
button_list[3] = gp_face4;
button_list[4] = gp_shoulderl;
button_list[5] = gp_shoulderlb;
button_list[6] = gp_shoulderr;
button_list[7] = gp_shoulderrb;
button_list[8] = gp_select;
button_list[9] = gp_start;
button_list[10] = gp_stickl;
button_list[11] = gp_stickr;
button_list[12] = gp_padu;
button_list[13] = gp_padd;
button_list[14] = gp_padl;
button_list[15] = gp_padr;

var axis_list = array_create(4, 0);
axis_list[0] = gp_axislh;
axis_list[1] = gp_axislv;
axis_list[2] = gp_axisrh;
axis_list[3] = gp_axisrv;

var button_name_list = array_create(16,0);
button_name_list[0] = "A"
button_name_list[1] = "B"
button_name_list[2] = "X"
button_name_list[3] = "Y"
button_name_list[4] = "Left Bumper"
button_name_list[5] = "Left Trigger"
button_name_list[6] = "Right Bumper"
button_name_list[7] = "Right Trigger"
button_name_list[8] = "Select"
button_name_list[9] = "Start"
button_name_list[10] = "Lstick Button"
button_name_list[11] = "Rstick Button"
button_name_list[12] = "Gpad Up"
button_name_list[13] = "Gpad Down"
button_name_list[14] = "Gpad Left"
button_name_list[15] = "Gpad Right"

var axis_name_list = array_create(4,0);
axis_name_list[0] = "Left Stick Horizontal"
axis_name_list[1] = "Left Stick Vertical"
axis_name_list[2] = "Right Stick Horizontal"
axis_name_list[3] = "Right Stick Vertical"


#endregion
#region Get Codes
//check all buttons for press
for (i = 0; i < array_length_1d(button_list); i++)
{
	if gamepad_button_check_pressed(gpad_id, button_list[i])
	{
		if keycode
		{
			return button_list[i];	
		}
		else
		{
			return button_name_list[i];
		}
	}
}

//check axis inputs
var deadzone = 0.2;
for (i = 0; i <array_length_1d(axis_list); i++)
{
	if abs(gamepad_axis_value(gpad_id, axis_list[i]))>deadzone
	{
		if keycode
		{
			return axis_list[i];
		}
		else
		{
			return axis_name_list[i];
		}	
	}
}
#endregion