//find lowest gamepad ID
// store result in gpid
if !variable_instance_exists(id,"gpad_active") gpad_active = false;
if !variable_instance_exists(id,"gpid") gpid = 0;

gpad_active = false;
var gp_num = gamepad_get_device_count();
for (var i = 0; i < gp_num; i++;){
	if gamepad_is_connected(i){
	   gpid = i;
	   gpad_active = true;
   }
}