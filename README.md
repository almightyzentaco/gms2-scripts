This is simply a repository to share and backup scripts I have made for
GameMaker 2. Feel free to use these in your own projects. No need to give
me attribution, but you may if you like.

If you need to reach me for any reason feel free to contact me:

joshuakoester@gmail.com
zentacoproductions@gmail.com
www.almightyzentaco.com
